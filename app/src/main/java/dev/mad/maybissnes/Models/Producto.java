package dev.mad.maybissnes.Models;

import java.util.ArrayList;

import java.util.List;

import dev.mad.maybissnes.R;

/**
 * Created by Daymel on 17/07/2017.
 */

public class Producto {
    private int idproducto;
    private String nombre;
    private float precioCompra;
    private float precioVenta;
    private int cantidadStock;
    private int cantidadSold;
    private String num_serie;
    private int categoria;
    private int imagen;




    public Producto(String nombre, float precioCompra, float precioVenta, int cantidadStock, int cantidadSold, String num_serie, int categoria, int imagen) {

        this.nombre = nombre;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
        this.cantidadStock = cantidadStock;
        this.cantidadSold = cantidadSold;
        this.num_serie = num_serie;
        this.categoria = categoria;

        this.imagen = imagen;

    }

    public Producto() {
    }





    public static final List<Producto> LAST_PRODUCTOS = new ArrayList<Producto>();

    static {
        LAST_PRODUCTOS.add(new Producto("Mono largo", 10, 12, 4,0, "1", 1, R.drawable.sample_mono));
        LAST_PRODUCTOS.add(new Producto("Blusa", 8, 10, 2,0, "1", 1, R.drawable.sample_blusa));
        LAST_PRODUCTOS.add(new Producto("Camisa", 18, 20, 3,0, "1", 1, R.drawable.sample_camisa));
        LAST_PRODUCTOS.add(new Producto("Blumer", 2, (float) 2.50, 4,0, "1", 1, R.drawable.sample_trusa));
        LAST_PRODUCTOS.add(new Producto("Audifonos", 15, 20, 15,0, "1", 2, R.drawable.sample_audifonos));
    }

    public int getCantidadSold() {
        return cantidadSold;
    }

    public void setCantidadSold(int cantidadSold) {
        this.cantidadSold = cantidadSold;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    public int getCantidadStock() {
        return cantidadStock;
    }

    public void setCantidadStock(int cantidadStock) {
        this.cantidadStock = cantidadStock;
    }

    public String getNum_serie() {
        return num_serie;
    }

    public void setNum_serie(String num_serie) {
        this.num_serie = num_serie;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }
}
