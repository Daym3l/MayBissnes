package dev.mad.maybissnes.cmp_Operaciones;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dev.mad.maybissnes.R;
import dev.mad.maybissnes.Utiles.DecoracionLineaDivisoria;
import dev.mad.maybissnes.Adapters.AdaptadorProductos;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductosListFragment extends Fragment {

    private RecyclerView recicler;
    private LinearLayoutManager layoutManager;
    private AdaptadorProductos adaptador;

    public ProductosListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_productos_list, container, false);

        recicler = (RecyclerView) v.findViewById(R.id.recicladorProductos);
        layoutManager = new LinearLayoutManager(getActivity());
        recicler.setLayoutManager(layoutManager);

        adaptador = new AdaptadorProductos();
        recicler.setAdapter(adaptador);
        recicler.addItemDecoration(new DecoracionLineaDivisoria(getActivity()));

        return v;
    }

}
