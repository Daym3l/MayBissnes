package dev.mad.maybissnes.cmp_Operaciones.Articulos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import dev.mad.maybissnes.Principal;
import dev.mad.maybissnes.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddArticleFragment extends Fragment {

    private SeekBar sb_Cantidad;

    public AddArticleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_article, container, false);

        sb_Cantidad = (SeekBar) v.findViewById(R.id.sb_cantidad);


        sb_Cantidad.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            private Toast toastStart = Toast.makeText(getActivity(), getText(R.string.start), Toast.LENGTH_SHORT);
            private Toast toastStop = Toast.makeText(getActivity(), getText(R.string.stop), Toast.LENGTH_SHORT);
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                toastStop.cancel();
                toastStart.setGravity(Gravity.TOP|Gravity.LEFT, 60, 110);
                toastStart.show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                toastStart.cancel();
                toastStop.setGravity(Gravity.TOP|Gravity.RIGHT, 60, 110);
                toastStop.show();
            }
        });

        return v;
    }


}
