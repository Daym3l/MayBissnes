package dev.mad.maybissnes.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import dev.mad.maybissnes.R;
import dev.mad.maybissnes.Models.Producto;

/**
 * Created by Daymel on 18/07/2017.
 */

public class AdaptadorProductos extends RecyclerView.Adapter<AdaptadorProductos.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre;
        public TextView categoria;
        public TextView precio;
        public TextView precioventa;
        public ImageView imagen;
        public TextView cantidad;


        public ViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.tv_nombre_producto);
            categoria = (TextView) v.findViewById(R.id.tv_categoria);
            precio = (TextView) v.findViewById(R.id.tv_precio_compra);
            precioventa = (TextView) v.findViewById(R.id.tv_precio_venta_valor);
            cantidad = (TextView) v.findViewById(R.id.tv_cantidad_productos);

            imagen = (ImageView) v.findViewById(R.id.iv_image_producto);
        }
    }

    public AdaptadorProductos() {
    }
    @Override
    public AdaptadorProductos.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_productos,parent,false);

        return new AdaptadorProductos.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdaptadorProductos.ViewHolder holder, int position) {
        Producto item = Producto.LAST_PRODUCTOS.get(position);
        String precio = String.valueOf(item.getPrecioCompra());
        String precioVenta = String.valueOf(item.getPrecioVenta());

        Glide.with(holder.itemView.getContext()).load(item.getImagen()).centerCrop().into(holder.imagen);
        holder.nombre.setText((item.getNombre()));
        holder.cantidad.setText(String.valueOf(item.getCantidadStock()));
        holder.categoria.setText("Categoría");
        holder.precio.setText("$"+precio);
        holder.precioventa.setText("$"+precioVenta);


    }

    @Override
    public int getItemCount() {
        return Producto.LAST_PRODUCTOS.size();
    }
}
