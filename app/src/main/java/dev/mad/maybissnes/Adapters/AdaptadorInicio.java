package dev.mad.maybissnes.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import dev.mad.maybissnes.R;
import dev.mad.maybissnes.Models.Producto;

/**
 * Created by Daymel on 17/07/2017.
 */

public class AdaptadorInicio extends RecyclerView.Adapter<AdaptadorInicio.ViewHolder> {


      public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre;
        public TextView precio;
        public ImageView imagen;

        public TextView decimal;

        public ViewHolder(View v) {
            super(v);
            nombre = (TextView) v.findViewById(R.id.nombre_producto);
            precio = (TextView) v.findViewById(R.id.precio_producto);

            decimal = (TextView) v.findViewById(R.id.tv_util_decimal);
            imagen = (ImageView) v.findViewById(R.id.miniatura_producto);
        }
    }

    public AdaptadorInicio() {
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_inicio,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Producto item = Producto.LAST_PRODUCTOS.get(position);

        Glide.with(holder.itemView.getContext()).load(item.getImagen()).centerCrop().into(holder.imagen);
        holder.nombre.setText((item.getNombre()));

        String precio = String.valueOf(item.getPrecioVenta());
        String[] arrayprecio = precio.split("\\.");

        holder.precio.setText(arrayprecio[0]);
        holder.decimal.setText("."+arrayprecio[1]+"0");

    }

    @Override
    public int getItemCount() {
        return Producto.LAST_PRODUCTOS.size();
    }
}
