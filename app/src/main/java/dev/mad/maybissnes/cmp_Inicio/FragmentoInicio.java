package dev.mad.maybissnes.cmp_Inicio;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



import dev.mad.maybissnes.Adapters.AdaptadorInicio;
import dev.mad.maybissnes.R;


public class FragmentoInicio extends Fragment {

    private RecyclerView recicler;
    private LinearLayoutManager layoutManager;
    private AdaptadorInicio adaptador;


    public FragmentoInicio() {

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragmento_inicio,container,false);

        recicler = (RecyclerView) v.findViewById(R.id.reciclador);
        layoutManager = new LinearLayoutManager(getActivity());
        recicler.setLayoutManager(layoutManager);

        adaptador = new AdaptadorInicio();
        recicler.setAdapter(adaptador);

        return v;
    }

}
