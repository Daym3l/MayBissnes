package dev.mad.maybissnes;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.SimpleCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import dev.mad.maybissnes.cmp_Categorias.Fragmento_categorias;
import dev.mad.maybissnes.cmp_Inicio.FragmentoInicio;
import dev.mad.maybissnes.cmp_Operaciones.Articulos.AddArticleFragment;
import dev.mad.maybissnes.cmp_Operaciones.OperacionesFragmentViewPager;

public class Principal extends AppCompatActivity {
    private final static String apk_version = BuildConfig.VERSION_NAME;
    public BoomMenuButton bmb;
    FragmentManager fragmentManager;
    Fragment fragmentoGenerico = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        bmb = (BoomMenuButton) findViewById(R.id.fab);
        assert bmb != null;
        fragmentManager = getSupportFragmentManager();

        bmb.addBuilder(new HamButton.Builder().normalImageRes(R.drawable.png_productos)
                .normalColorRes(R.color.title_ropa)
                .normalTextRes(R.string.boom_Text_Productos)
                .subNormalTextRes(R.string.boom_Text_ProductosSub)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        seleccionarOperacion(index);
                    }
                }));
        bmb.addBuilder(new HamButton.Builder().normalImageRes(R.drawable.png_ventas)
                .normalColorRes(R.color.sold)
                .normalTextRes(R.string.boom_Text_Ventas)
                .subNormalTextRes(R.string.boom_Text_VentasSub)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        seleccionarOperacion(index);
                    }
                }));
        bmb.addBuilder(new HamButton.Builder().normalImageRes(R.drawable.png_entregar)
                .normalColorRes(R.color.price)
                .normalTextRes(R.string.boom_Text_Entregas)
                .subNormalTextRes(R.string.boom_Text_EntregasSub)
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        seleccionarOperacion(index);
                    }
                }));


        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (navigationView != null) {
            prepararDrawer(navigationView);
            // Seleccionar item por defecto
            seleccionarItem(navigationView.getMenu().getItem(0));
        }

        View vistaNav = navigationView.getHeaderView(0);
        TextView tv_Version = (TextView) vistaNav.findViewById(R.id.tv_version_valor);
        tv_Version.setText(apk_version);


    }

    private void prepararDrawer(NavigationView navigationView) {
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        seleccionarItem(menuItem);
                        drawer.closeDrawers();
                        return true;
                    }
                });

    }

    private void seleccionarOperacion(int index) {
        Fragment fragmentoOperaciones = null;

        String title = "";
        switch (index) {
            case 0:
                fragmentoOperaciones = new AddArticleFragment();
                bmb.setVisibility(View.INVISIBLE);
                title = "Nuevo ariculo";
                break;
        }

        if (fragmentoOperaciones != null) {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.cl_contenedor_principal, fragmentoOperaciones)
                    .commit();
        }
        setTitle(title);

    }

    private void seleccionarItem(MenuItem itemDrawer) {

        Fragment containerFragment = getSupportFragmentManager().findFragmentById(R.id.cl_contenedor_principal);


        switch (itemDrawer.getItemId()) {
            case R.id.nav_inicio:
                fragmentoGenerico = new FragmentoInicio();
                bmb.setVisibility(View.INVISIBLE);
                break;
            case R.id.nav_operaciones:
                fragmentoGenerico = new OperacionesFragmentViewPager();
                bmb.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_categorias:
                fragmentoGenerico = new Fragmento_categorias();
                bmb.setVisibility(View.INVISIBLE);
                break;
            case R.id.nav_deudas:
                // Iniciar actividad de configuración
                break;
        }
        if (containerFragment == null) {
            fragmentManager
                    .beginTransaction()
                    .add(R.id.cl_contenedor_principal, fragmentoGenerico)
                    .commit();
        } else {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.cl_contenedor_principal, fragmentoGenerico).addToBackStack(null)
                    .commit();
        }

        // Setear título actual
        setTitle(itemDrawer.getTitle());
    }

    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (count > 0) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.app_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
